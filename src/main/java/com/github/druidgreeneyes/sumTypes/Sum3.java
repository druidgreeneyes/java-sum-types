package com.github.druidgreeneyes.sumTypes;

import java.util.function.Function;

public abstract class DU<tA, tB, tC> {
    private DU(){}

    public final ProjectionA<tA, tB, tC> a() {
        return new ProjectionA<>(this);
    }

    public final ProjectionB<tA, tB, tC> b() {
        return new ProjectionB<>(this);
    }

    public final ProjectionC<tA, tB, tC> c() {
        return new ProjectionC<>(this);
    }

    public abstract boolean isA();
    public abstract boolean isB();
    public abstract boolean isC();

    public abstract <tR> tR retrieve(
            final Function<tA, tR> fA,
            final Function<tB, tR> fB,
            final Function<tC, tR> fC
    );

    public final <tX, tY, tZ> DU<tX, tY, tZ> outerMap(
            final Function<tA, tX> fA,
            final Function<tB, tY> fB,
            final Function<tC, tZ> fC
    ) {
        return retrieve(fA.andThen(at()), fB.andThen(bt()), fC.andThen(ct()));
    }
    
    private static final class A<tA, tB, tC> extends DU<tA, tB, tC> {
        private final tA a;

        A(final tA a) {
            this.a = a;
        }

        public boolean isA() { return true; }
        public boolean isB() { return false; }
        public boolean isC() { return false; }

        public <tR> tR retrieve(
            final Function<tA, tR> fA,
            final Function<tB, tR> fB,
            final Function<tC, tR> fC) {
            return fA.apply(a);
        }
    }

    private static final class B<tA, tB, tC> extends DU<tA, tB, tC> {
        private final tB b;

        B(final tB b) {
            this.b = b;
        }

        public boolean isA() { return true; }
        public boolean isB() { return false; }
        public boolean isC() { return false; }

        public <tR> tR retrieve(
            final Function<tA, tR> fA,
            final Function<tB, tR> fB,
            final Function<tC, tR> fC) {
            return fB.apply(b);
        }
    }

    private static final class C<tA, tB, tC> extends DU<tA, tB, tC> {
        private final tC c;

        C(final tC c) {
            this.c = c;
        }

        public boolean isA() { return true; }
        public boolean isB() { return false; }
        public boolean isC() { return false; }

        public <tR> tR retrieve(
            final Function<tA, tR> fA,
            final Function<tB, tR> fB,
            final Function<tC, tR> fC) {
            return fC.apply(c);
        }
    }

    public static <tA, tB, tC> DU<tA, tB, tC> a(final tA a) {
        return new A<>(a);
    }

    public static <tA, tB, tC> Function<tA, DU<tA, tB, tC>> at() {
        return DU::a;
    }

    public static <tA, tB, tC> DU<tA, tB, tC> b(final tB b) {
        return new B<>(b);
    }

    public static <tA, tB, tC> Function<tB, DU<tA, tB, tC>> bt() {
        return DU::b;
    }

    public static <tA, tB, tC> DU<tA, tB, tC> c(final tC c) {
        return new C<>(c);
    }

    public static <tA, tB, tC> Function<tC, DU<tA, tB, tC>> ct() {
        return DU::c;
    }


}
